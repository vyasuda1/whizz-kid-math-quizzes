package com.company;

import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.NumberFormat;
import java.util.*;

public class UIFrame extends Container {

    /**
     * Declaration of all components for entire form.
     * All components are labeled with their main panel type as the start of the variable name to differentiate which variables belong to which panel.
     * E.g. Login page has a button that says login, this buttons variable name is loginButton.
     * Variables for each component can be viewed in the UIFrame.form file. Clicking the component will show "field name" which is the variable name for that component.
     */

    private Student thisAccount;
    private int quizType, correctCounter, incorrectCounter, typoCounter;

    private JPanel MainPanel;

    private JPanel CreateAcct;
    private JPanel createPanel;
    private JPanel createButtonPanel;
    private JLabel createAccountText;
    private JLabel createQ1Text;
    private JLabel createQ2Text;
    private JLabel createQ3Text;
    private JLabel createSQ1;
    private JLabel createSQ2;
    private JLabel createSQ3;
    private JLabel createUNLabel;
    private JLabel createPWLabel;
    private JLabel createPWLabel2;
    private JTextField createUN;
    private JTextField createPassword1;
    private JTextField createPassword2;
    private JTextField createQ1Answer;
    private JTextField createQ2Answer;
    private JTextField createQ3Answer;
    private JButton createAcctButton;



    private JPanel Menu;
    private JPanel menuPanel;
    private JPanel menuTestPanel;
    private JPanel menuBottomPanel;
    private JPanel menuNestedPanel;
    private JLabel menuOptionsText;
    private JButton menuAdditionTestButton;
    private JButton menuSubtractionTestButton;
    private JButton menuMultiplicationTestButton;
    private JButton menuDivisionTestButton;
    private JButton menuAccountSettingsButton;
    private JButton menuLogoutButton;
    private JButton menuStatsButton;

    private JPanel ResetPW;
    private JPanel resetPanel;
    private JPanel resetPanel2;
    private JPanel resetPanel3;
    private JPanel resetTextPanel;
    private JLabel resetPWText;
    private JLabel resetUN;
    private JLabel resetQ1;
    private JLabel resetQ3;
    private JLabel resetQ2;
    private JLabel resetNewPW;
    private JLabel resetNewPW2;
    private JTextField resetUserQ1;
    private JTextField resetUserUN;
    private JTextField resetUserQ3;
    private JTextField resetUserQ2;
    private JTextField resetUserNewPW;
    private JTextField resetUserNewPW2;
    private JButton resetResetPWButton;


    private JPanel Stats;
    private JPanel statsPanel;
    private JLabel statsText;
    private JButton statsReturnButton;

    private JPanel Test;
    private JPanel testPanel;
    private JPanel testQPanel;
    private JLabel testTextField;
    private JLabel testCurrentQ;
    private JLabel testTimer;
    private JLabel testTimerLabel;
    private JTextField testCurrentA;
    private JButton testEnterAns;


    private JPanel Login;
    private JPanel loginPanel;
    private JPanel loginPanel3;
    private JPanel loginPanel2;
    private JPanel loginPanel4;
    private JPanel loginPanel5;
    private JLabel loginProgramText;
    private JLabel loginUNLabel;
    private JLabel loginPWLabel;
    private JPasswordField loginPW;
    private JTextField loginUN;
    private JButton loginButton;
    private JButton loginForgotPWButton;
    private JButton loginCreateAcctButton;


    private JPanel AccountSettings;
    private JPanel accountCenterPanel;
    private JLabel accountSettingsLabel;
    private JLabel accountUN;
    private JLabel accountChangePW1;
    private JLabel accountChangePW2;
    private JTextField accountUserUN;
    private JTextField accountUserPW;
    private JTextField accountUserPW2;
    private JButton accountReturnToMenu;
    private JLabel testAnswerLabel;
    private JButton goBackToMain;
    private JPanel Results;
    private JButton closeButton;
    private JPanel closeButtonPanel;
    private JLabel resultsLabel;
    private JLabel typoLabel;
    private JLabel incorrectLabel;
    private JLabel levelUpLabel;
    private JLabel correctLabel;
    private JButton accountChangePWButton;
    private JTextField accountVerifyPWText;
    private JLabel accountVerifyPWLabel;
    private JLabel displayLabel;
    private JLabel additionLabel;
    private JLabel subtractionLabel;
    private JLabel multiplicationLabel;
    private JLabel divisionLabel;
    private JLabel hyperLink;
    private JLabel statsAdd;
    private JLabel statsSub;
    private JLabel statsMult;
    private JLabel statsDiv;
    private JLabel correctUserAnswers;


    /**
     * Integer for timer in test panel
     */
    int k = 15;
    int answer, x, y;
    CardLayout cards = new CardLayout();


    javax.swing.Timer t = new javax.swing.Timer(1000, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            testTimer.setText(String.valueOf(k) + " s");
            k--;
        }
    });

    public class TimedExit {
        java.util.Timer timer = new java.util.Timer();
        TimerTask exitApp = new TimerTask() {
            public void run() {
                //instead of go to main panel, go to results panel
                correctLabel.setText(String.valueOf("CORRECT: " + correctCounter));
                incorrectLabel.setText(String.valueOf("INCORRECT: " + incorrectCounter));
                typoLabel.setText(String.valueOf("TYPOS: " + typoCounter));

                int level = thisAccount.getLevel(quizType);
                levelUpLabel.setText("No level change! Current level: " + thisAccount.getLevel(quizType));

                if(correctCounter > 5 && incorrectCounter + typoCounter < 5) {
                    level++;
                    thisAccount.setLevel(quizType, level);
                    levelUpLabel.setText("Level up! New level: " + thisAccount.getLevel(quizType));
                }
                else if(incorrectCounter + typoCounter > 5 || correctCounter < (incorrectCounter + typoCounter)) {
                    level--;
                    thisAccount.setLevel(quizType, level);
                    levelUpLabel.setText("Level down! New level: " + thisAccount.getLevel(quizType));
                }
                cards.show(MainPanel, "Results");

                t.stop();
                k = 15;
                testAnswerLabel.setText("");

                thisAccount.saveQuizAttempt(quizType, correctCounter, incorrectCounter, typoCounter);
                thisAccount.saveStudent();
                //move to results frame
                correctCounter = 0;
                incorrectCounter = 0;
                typoCounter = 0;
            }
        };
        public TimedExit() {
            timer.schedule(exitApp, new Date(System.currentTimeMillis()+17*1000));
        }

    }



    public UIFrame() {

        //load the student file

        /**
         * Layout manager chosen for entire program. Each Panel contains multiple components and is visible one at a time.
         */
        MainPanel.setLayout(cards);

        /**
         * Adds all main panels to the MainPanel which is set to CardLayout format.
          */

        MainPanel.add(Login, "Login");
        MainPanel.add(CreateAcct, "Create");
        MainPanel.add(Menu, "Menu");
        MainPanel.add(ResetPW, "Reset");
        MainPanel.add(Stats, "Stats");
        MainPanel.add(Test, "Test");
        MainPanel.add(AccountSettings, "Account");
        MainPanel.add(Results, "Results");
        /**
         * Show Login panel at start of program
         */
        cards.show(MainPanel, "Login");


        /**
         * Create frame and and MainPanel. Makes frame visible.
         */

        JFrame frame = new JFrame("Whizz Kid Math Quizzes");
        frame.add(MainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(700,700);
        frame.setVisible(true);


        /**
         * Action listeners below are methods that are called when buttons from the panels are clicked.
         * Code listed in each action listener will execute upon being clicked.
         */


        /**
         * This method changes the panel from Login to Create Account
         */

        loginCreateAcctButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(MainPanel, "Create");
            }
        });

        /**
         * This method creates a new student account
         * Need method in student class to add security questions
         */
        createAcctButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                thisAccount = new Student(createUN.getText(), createPassword1.getText(), createQ1Answer.getText(), createQ2Answer.getText(), createQ3Answer.getText());
                cards.show(MainPanel, "Login");

            }
        });

        /**
         * This method logs the user in. Should check for input validation.
         */

        loginButton.addActionListener(new ActionListener() {
            //Viola
            @Override
            public void actionPerformed(ActionEvent e) {
                //check login info from saved user data?

                String checkPw = loginPW.getText();
                String checkUn = loginUN.getText();


                String fileName = checkUn + ".txt";

                File file = new File(fileName);

                if(file.exists()) {
                    thisAccount = new Student(file);
                    if(thisAccount.getPassword().equals(checkPw) && thisAccount.getName().equals(checkUn)) {
                        cards.show(MainPanel, "Menu");
                        loginUN.setText("");
                    }
                    else
                        JOptionPane.showMessageDialog(null, "Invalid password");
                }
                else {
                    JOptionPane.showMessageDialog(null, "Invalid username.");
                }


                loginPW.setText("");

            }
        });

        /**
         * This method brings the user to the stats panel
         */

        menuStatsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                displayLabel.setText("Quiz Types        Level");
                additionLabel.setText("Addition            " + Integer.toString(thisAccount.getLevel(0)));
                subtractionLabel.setText("Subtraction         " + Integer.toString(thisAccount.getLevel(1)));
                multiplicationLabel.setText("Multiplication      " + Integer.toString(thisAccount.getLevel(2)));
                divisionLabel.setText("Division            " + Integer.toString(thisAccount.getLevel(3)));

                cards.show(MainPanel, "Stats");

            }
        });


        /**
         * Go back to main menu
         */
        goBackToMain.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(MainPanel, "Login");
            }
        });

        /**
         * This method changes the users password from the Reset panel then redirects to the login page.
         */
        resetResetPWButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String verifyUsername = resetUserUN.getText();
                String newPassword = resetUserNewPW.getText();
                String verifyNewPassword = resetUserNewPW2.getText();
                String securityQuestion1 = resetUserQ1.getText();
                String securityQuestion2 = resetUserQ2.getText();
                String securityQuestion3 = resetUserQ3.getText();
                String fileName = verifyUsername + ".txt";
                File file = new File(fileName);
                if(file.exists()) {
                    thisAccount = new Student(file);
                    String currentQ1 = thisAccount.getAnswer("In what city were you born?");
                    String currentQ2 = thisAccount.getAnswer("What is your mother's maiden name?");
                    String currentQ3 = thisAccount.getAnswer("How many siblings do you have?");;
                    boolean q1IsRight = securityQuestion1.equals(currentQ1);
                    boolean q2IsRight = securityQuestion2.equals(currentQ2);
                    boolean q3IsRight = securityQuestion3.equals(currentQ3);
                    if (q1IsRight && q2IsRight && q3IsRight) {
                        if (newPassword.equals(verifyNewPassword)) {
                            //save the updated password **************
                            thisAccount.setPassword(newPassword);
                            JOptionPane.showMessageDialog(null, "Password is updated");
                        }
                        else {
                            JOptionPane.showMessageDialog(null, "Passwords don't match");
                        }
                    }
                    else {
                        JOptionPane.showMessageDialog(null, "One or more security questions are incorrect");
                    }
                }
                else {
                    JOptionPane.showMessageDialog(null, "Invalid username.");
                }
            }
        });

        /**
         * This method logs the user out and redirects to the Login panel
         */

        menuLogoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(MainPanel, "Login");
            }
        });

        /**
         * This method brings the user to their account settings and will allow them to change their password
         */

        menuAccountSettingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                accountUserUN.setText(thisAccount.getName());
                cards.show(MainPanel, "Account");
            }
        });

        /**
         * This method returns the user to the main menu from the Account Settings panel
         */

        accountReturnToMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(MainPanel, "Menu");
            }
        });

        /**
         * This method returns the user to the main menu from the Stats panel
         */

        statsReturnButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(MainPanel, "Menu");
            }
        });

        /**
         * This method will allow the user to reset their password from the login page.
         * Will require the proper input validation of their security questions
         */

        loginForgotPWButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(MainPanel, "Reset");
            }
        });

        /**
         * Allows the user to hit enter to submit answer to question
         */
        testCurrentA.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER) {
                    MathQuiz quiz;

                    quiz = switch(quizType){
                        case 0 -> new AdditionQuiz(thisAccount);
                        case 1 -> new SubtractionQuiz(thisAccount);
                        case 2 -> new MultiplicationQuiz(thisAccount);
                        case 3 -> new DivisionQuiz(thisAccount);
                        default -> null;
                    };

                    try {
                        Integer.parseInt(testCurrentA.getText());
                        if(quiz.getAnswer(x,y) == Integer.parseInt(testCurrentA.getText())) {
                            testAnswerLabel.setText("Previous Question Correct");
                            correctCounter++;
                        }
                        else {
                            testAnswerLabel.setText("Previous Question Incorrect");
                            incorrectCounter++;
                        }
                    }
                    catch (NumberFormatException exc) {
                        testAnswerLabel.setText("Invalid input entered");
                        typoCounter++;
                    }

                    Random rand = new Random();
                    if(quizType == 3){
                        x = rand.nextInt(thisAccount.getLevel(3) + 10); //for a random integer between 0 and the difficulty level
                        y = rand.nextInt(thisAccount.getLevel(3)) + 1;
                        x*=y;
                    }
                    else {
                        x = rand.nextInt(101); //for a random integer between 0 and the difficulty level
                        y = rand.nextInt(quiz.getLevel() + 1);
                    }
                    testCurrentQ.setText(quiz.printQuestion(x,y));
                    testCurrentA.setText("");
                }
            }
        });
        /**
         * This method will retrieve the answer the user enters and check for correctness.
         * Will need to retrieve data from text field.
         * Potentially save all correct answers and questions at the end for student to see their results?
         */
        testEnterAns.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MathQuiz quiz;

                quiz = switch(quizType){
                    case 0 -> new AdditionQuiz(thisAccount);
                    case 1 -> new SubtractionQuiz(thisAccount);
                    case 2 -> new MultiplicationQuiz(thisAccount);
                    case 3 -> new DivisionQuiz(thisAccount);
                    default -> null;
                };

                try {
                    Integer.parseInt(testCurrentA.getText());
                    if(quiz.getAnswer(x,y) == Integer.parseInt(testCurrentA.getText())) {
                        testAnswerLabel.setText("Previous Question Correct");
                        correctCounter++;
                    }
                    else {
                        testAnswerLabel.setText("Previous Question Incorrect");
                        incorrectCounter++;
                    }
                }
                catch (NumberFormatException exc) {
                    testAnswerLabel.setText("Invalid input entered");
                    typoCounter++;
                }

                Random rand = new Random();
                if(quizType == 3){
                    x = rand.nextInt(thisAccount.getLevel(3) + 10); //for a random integer between 0 and the difficulty level
                    y = rand.nextInt(thisAccount.getLevel(3)) + 1;
                    x*=y;
                }
                else {
                    x = rand.nextInt(101); //for a random integer between 0 and the difficulty level
                    y = rand.nextInt(quiz.getLevel() + 1);
                }
                testCurrentQ.setText(quiz.printQuestion(x,y));
                testCurrentA.setText("");
            }
        });

        /**
         * This method will start an addition test
         */

        menuAdditionTestButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                quizType = 0;
                cards.show(MainPanel, "Test");

                MathQuiz quiz = new AdditionQuiz(thisAccount);
                Random rand = new Random();

                int answer;
                x = rand.nextInt(101); //for a random integer between 0 and the difficulty level
                y = rand.nextInt(thisAccount.getLevel(0) + 1);

                testCurrentQ.setText(quiz.printQuestion(x,y));
                t.start();
                TimedExit te = new TimedExit();

            }
        });

        menuSubtractionTestButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                quizType = 1;
                cards.show(MainPanel, "Test");
                MathQuiz quiz = new SubtractionQuiz(thisAccount);
                Random rand = new Random();

                int answer;
                x = rand.nextInt(101); //for a random integer between 0 and the difficulty level
                y = rand.nextInt(thisAccount.getLevel(1) + 1);

                testCurrentQ.setText(quiz.printQuestion(x,y));
                t.start();
                TimedExit te = new TimedExit();

            }
        });
        menuMultiplicationTestButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                quizType = 2;
                cards.show(MainPanel, "Test");

                MathQuiz quiz = new MultiplicationQuiz(thisAccount);
                Random rand = new Random();

                int answer;
                x = rand.nextInt(101); //for a random integer between 0 and the difficulty level
                y = rand.nextInt(thisAccount.getLevel(2) + 1);

                testCurrentQ.setText(quiz.printQuestion(x,y));
                t.start();
                TimedExit te = new TimedExit();

            }
        });
        menuDivisionTestButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                quizType = 3;
                cards.show(MainPanel, "Test");

                MathQuiz quiz = new DivisionQuiz(thisAccount);
                Random rand = new Random();

                int answer;

                x = rand.nextInt(thisAccount.getLevel(3) + 10); //for a random integer between 0 and the difficulty level
                y = rand.nextInt(thisAccount.getLevel(3)) + 1;
                x*=y;

                testCurrentQ.setText(quiz.printQuestion(x,y));
                t.start();
                TimedExit te = new TimedExit();

            }
        });

        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(MainPanel, "Menu");
            }
        });
        accountChangePWButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(accountVerifyPWText.getText().equals(thisAccount.getPassword())) {
                    if (accountUserPW.getText().equals(accountUserPW2.getText())) {
                        JOptionPane.showMessageDialog(null, "Password was successfully changed!");
                        thisAccount.setPassword(accountUserPW2.getText());
                    }
                    else
                        JOptionPane.showMessageDialog(null, "Passwords do not match.");
                }
                else
                    JOptionPane.showMessageDialog(null, "Incorrect password.");
            }
        });
        hyperLink.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    Desktop.getDesktop().browse(new URI(thisAccount.getName() + "-quiz-history.html"));

                } catch (IOException | URISyntaxException e1) {
                    JOptionPane.showMessageDialog(null, "No current quiz history!");
                }
            }
        });
    }
}
