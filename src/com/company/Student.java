package com.company;
import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * This class represents a student using the math program.
 * @author Viola Yasuda
 * @version 3/28/2021
 */
public class Student {
    //instance variables
    private String name; //aka username
    private String password;
    private String sq1, sq2, sq3;
    private int[] levels;
    private Map<String, String> securityQuestions;
    private String accountInfoFileName;
    private String quizHistoryFileName;

    /**
     * Constructor for Student class. Use for new students.
     * @param name the username of the student
     * @param password the password for the student's account
     * @param answer1 the answer to the city question
     * @param answer2 the answer to mother question
     * @param answer3 the answer to the siblings question
     */
    public Student(String name, String password, String answer1, String answer2, String answer3) {
        this.name = name;
        this.password = password;
        levels = new int[]{1, 1, 1, 1};
        securityQuestions = new LinkedHashMap<>();
        securityQuestions.put("In what city were you born?", answer1);
        securityQuestions.put("What is your mother's maiden name?", answer2);
        securityQuestions.put("How many siblings do you have?", answer3);
        accountInfoFileName = name + ".txt";
        quizHistoryFileName = name + "-quiz-history.html";
        saveStudent();
    }

    /**
     * Constructor for Student class. Use for returning students.
     * @param file the student file name
     */
    public Student(File file) {
        String answer1, answer2, answer3;
        try {
            Scanner in = new Scanner(file);
            name = in.next();
            password = in.next();
            levels = new int[4];
            levels[0] = in.nextInt();
            levels[1] = in.nextInt();
            levels[2] = in.nextInt();
            levels[3] = in.nextInt();
            in.nextLine();
            answer1 = in.nextLine();
            answer2 = in.nextLine();
            answer3 = in.nextLine();
            securityQuestions = new LinkedHashMap<>();
            securityQuestions.put("In what city were you born?", answer1);
            securityQuestions.put("What is your mother's maiden name?", answer2);
            securityQuestions.put("How many siblings do you have?", answer3);
            accountInfoFileName = name + ".txt";
            quizHistoryFileName = name + "-quiz-history.html";
            in.close();
        }
        catch(FileNotFoundException e){
                e.printStackTrace();
        }
    }

    /**
     * This method saves info from a quiz attempt to a file
     * @param correct number of correct answers during quiz attempt
     * @param incorrect number of incorrect answers during quiz attempt
     * @param typos number of typos during quiz attempt
     */
    public void saveQuizAttempt(int quizType, int correct, int incorrect, int typos) {
        try {
            String quizTypeDisplay = "";

            quizTypeDisplay = switch(quizType) {
                case 0 -> "Addition";
                case 1 -> "Subtraction";
                case 2 -> "Multiplication";
                case 3 -> "Division";
                default -> null;
            };

            File file = new File(quizHistoryFileName);
            FileWriter fWriter = new FileWriter(quizHistoryFileName, file.exists());
            PrintWriter outputFile = new PrintWriter(fWriter);
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();
            outputFile.println("Date and Time of Quiz Attempt: " + dtf.format(now) + "<br>");
            outputFile.println("Quiz Type: " + quizTypeDisplay + "<br>");
            outputFile.println("Number of questions answered correctly: " + correct + "<br>");
            outputFile.println("Number of questions answered incorrectly: " + incorrect + "<br>");
            outputFile.println("Number of typos made during quiz attempt: " + typos + "<br>");
            outputFile.println("Level After Quiz Attempt: " + levels[quizType] + "<br>");
            outputFile.println("<br>");
            outputFile.close();
        }
        catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * This method returns the student's name
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * This method returns the student's password
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * This method returns the array holding the student's levels.
     * @return levels
     */
    public int[] getLevels() {
        return levels;
    }

    /**
     * This method returns the answer to a security question.
     * @param question the question to receive the answer from
     * @return the answer to the security question
     */
    public String getAnswer(String question) {
        return securityQuestions.get(question);
    }

    /**
     * This method returns the level stored at a particular index
     * @param index the index the level is stored at in levels
     * @return the value of the level at index
     */
    public int getLevel(int index) {
        if (index >= 0 && index < levels.length)
            return levels[index];
        else
            return 1;
    }

    /**
     * This method updates the student's name.
     * @param newName the new name of the student
     */
    public void setName(String newName) {
        name = newName;
    }

    /**
     * This method updates the student's password.
     * @param newPassword the student's new password
     */
    public void setPassword(String newPassword) {
        password = newPassword;
        saveStudent();
    }

    /**
     * This method updates all of the student's levels.
     * @param newLevels the new levels of the student
     */
    public void setLevels(int[] newLevels) {
        levels = newLevels;
    }

    public void setLevel(int index, int number) {
        if (number > 0)
            levels[index] = number;
        else
            levels[index] = 1;
    }
    /**
     * Saves the data to a file
     */
    public void saveStudent() {
        try(PrintWriter pw = new PrintWriter(accountInfoFileName)) {
            pw.println(name);
            pw.println(password);
            for(int level : levels) {
                pw.println(level);
            }
            for(String key : securityQuestions.keySet()) {
                pw.println(securityQuestions.get(key));
            }
        }
        catch(FileNotFoundException e) {
            e.fillInStackTrace();
        }
        catch (NullPointerException b) {
            b.fillInStackTrace();
        }
    }
}
