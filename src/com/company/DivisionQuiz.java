package com.company;

import java.util.Random;
import java.util.Scanner;

/**
 * This class generates division quizzes.
 */
public class DivisionQuiz extends MathQuiz {
    /**
     * Constructor for DivisionQuiz.
     * @param student the student being quizzed.
     */
    public DivisionQuiz(Student student) {
        super(student, 3, "Division", "/");
    }

    /**
     * Starts the quiz.
     */
    @Override
    public void startQuiz() {
        Scanner in = new Scanner(System.in);
        Random rand = new Random();

        int answer, x, y = 1;
        int difficulty = getLevel();
        System.out.printf("Difficulty: %d \n", difficulty);

        long start = System.currentTimeMillis(); //start time (in milli-seconds)
        long end = start + 60000; // end time = start time + 60 seconds, 1 second = 1000

        while (System.currentTimeMillis() < end) {
            x = rand.nextInt(difficulty + 10); //for a random integer between 0 and the difficulty level + 10
            y = rand.nextInt(difficulty) + 1; //sets y to a rand number between 1 to x
            x *= y;
            System.out.println(printQuestion(x, y));

            while (!in.hasNextInt()) {
                String input = in.next();
                System.out.printf("%s is not a number!\n", input);
                printQuestion(x, y);
            }
            answer = in.nextInt();
            if (answer != getAnswer(x, y)) {
                System.out.printf("Incorrect. The correct answer is: %d \n", getAnswer(x, y));
            } else {
                System.out.println("Correct.");
            }
            long currentTime = end - System.currentTimeMillis();
            System.out.printf("%.1f seconds left.\n", (double)currentTime / 1000);
        }
        System.out.println("Out of time.");
    }

    /**
     * Calculates the quotient of two numbers.
     * @param num1 the first number in the operation being asked
     * @param num2 the second number in the operation being asked
     * @return the quotient of num1 and num2
     */
    @Override
    protected int getAnswer(int num1, int num2) {
        return num1 / num2;
    }
}
