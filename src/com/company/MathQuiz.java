package com.company;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import java.util.Scanner;

/**
 * This class sets up a math quiz for the student.
 * @author Dimitar Dimitrov
 * @version 3/28/2021
 */
public class MathQuiz {
    //instance variables
    private String quizType, operand;
    private int level, index;


    /**
     * Constructor for MathQuiz class.
     * @param student the student account object
     * @param index the index to store the student's level
     */
    public MathQuiz(Student student, int index, String quizType, String operand) {
        level = student.getLevel(index);
        this.index = index;
        this.quizType = quizType;
        this.operand = operand;
    }

    /**
     * Sets the level of difficulty for the quiz.
     * @param level the level of difficulty
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * Gets the level of difficulty of this quiz.
     * @return the difficulty level
     */
    public int getLevel() {
        return level;
    }

    /**
     * Gets the index the quiz's level is stored at.
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * Gets the type of quiz questions this quiz generates.
     * @return quiz type
     */
    public String getQuizType() {
        return quizType;
    }

    /**
     * Returns the operand used in the math quiz
     * @return operand
     */
    public String getOperand() {
        return operand;
    }

    /**
     * Starts the math quiz.
     */
    public void startQuiz() {
        Scanner in = new Scanner(System.in);
        Random rand = new Random();

        int answer, x, y;
        int difficulty = getLevel();
        System.out.printf("Difficulty: %d \n", difficulty);

        long start = System.currentTimeMillis(); //start time (in milli-seconds)
        long end = start + 60000; // end time = start time + 60 seconds, 1 second = 1000

        while (System.currentTimeMillis() < end) {
            x = rand.nextInt(101); //for a random integer between 0 and the difficulty level
            y = rand.nextInt(difficulty + 1);
            System.out.println(printQuestion(x,y));

            while(!in.hasNextInt()) {
                String input = in.next();
                System.out.printf("%s is not a number!\n", input);
                printQuestion(x, y);
            }
            answer = in.nextInt();
            if(answer != getAnswer(x,y)) {
                System.out.printf("Incorrect. The correct answer is: %d \n", getAnswer(x,y));
            }
            else {
                System.out.println("Correct.");
            }
            long currentTime = end - System.currentTimeMillis();
            System.out.printf("%.1f seconds left.\n", (double)currentTime / 1000);
        }
        System.out.println("Out of time.");
    }

    /**
     * Returns a quiz question.
     * @param num1 the first number in the operation being asked
     * @param num2 the second number in the operation being asked
     * @return the quiz question
     */
    public String printQuestion(int num1, int num2) {
        return (num1 + " " + operand + " " + num2 + " = ");
    }

    /**
     * Calculates the answer for a question using two numbers.
     * @param num1 the first number in the operation being asked
     * @param num2 the second number in the operation being asked
     * @return the result of the operation being asked
     */
    protected int getAnswer(int num1, int num2) {
        return 0;
    }
}
