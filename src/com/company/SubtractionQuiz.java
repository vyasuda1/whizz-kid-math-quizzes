package com.company;

/**
 * This class generates subtraction quizzes.
 */
public class SubtractionQuiz extends MathQuiz {
    /**
     * Constructor for SubtractionQuiz.
     * @param student the student being quizzed
     */
    public SubtractionQuiz(Student student) {
        super(student, 1, "Subtraction", "-");
    }

    /**
     * Calculates the difference of two numbers.
     * @param num1 the first number in the operation being asked
     * @param num2 the second number in the operation being asked
     * @return the difference of num1 and num2
     */
    @Override
    protected int getAnswer(int num1, int num2) {
        return num1 - num2;
    }
}
