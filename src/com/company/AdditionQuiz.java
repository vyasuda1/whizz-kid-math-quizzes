package com.company;
import java.util.Timer;

/**
 * This class generates addition math quizzes.
 */
public class AdditionQuiz extends MathQuiz {
    //instance variable
    private static int seconds = 60;
    static Timer timer = new Timer();

    /**
     * Constructor for AdditionQuiz.
     * @param student the student being quizzed
     */
    public AdditionQuiz(Student student) {
        super(student, 0, "Addition", "+");
    }

    /**
     * Calculates the sum of two numbers.
     * @param num1 the first number in the operation being asked
     * @param num2 the second number in the operation being asked
     * @return the sum of num1 and num2
     */
    @Override
    protected int getAnswer(int num1, int num2) {
        return num1 + num2;
    }
}
