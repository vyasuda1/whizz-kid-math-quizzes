package com.company;
import java.util.Timer;
import java.util.TimerTask;

public class Clock {
    static int seconds = 3;
    static Timer timer = new Timer();

    public void beginClock() {
        TimerTask test = new TimerTask() {
            public void run() {
                decrementTime();
                getTime();
            }
        };
        timer.scheduleAtFixedRate(test, 1000, 2000);
    }

    private static final int decrementTime() {
        if (seconds == 3) {
            System.out.println("THREE");
        }
        else if(seconds == 2) {
            System.out.println("TWO");
        }
        else if(seconds == 1){
            System.out.println("ONE");
        }
        else {
            System.out.println("BEGIN");
            timer.cancel();
        }
        return seconds--;
    }

    private static int getTime(){
        return seconds;
    }
}
