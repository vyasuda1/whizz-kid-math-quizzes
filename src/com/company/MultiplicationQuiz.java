package com.company;

/**
 * This class generates multiplication quizzes.
 */
public class MultiplicationQuiz extends MathQuiz {
    /**
     * Constructor for MultiplicationQuiz.
     * @param student the student being quizzed.
     */
    public MultiplicationQuiz(Student student) {
        super(student, 2, "Multiplication", "*");
    }

    /**
     * Calculates the product of two numbers.
     * @param num1 the first number in the operation being asked
     * @param num2 the second number in the operation being asked
     * @return the product of num1 and num2
     */
    @Override
    protected int getAnswer(int num1, int num2) {
        return num1 * num2;
    }
}
